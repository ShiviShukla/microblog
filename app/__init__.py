from flask import Flask
from config import Config  # just importing configuration from seperate file for security purpose

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager    # for Login

import logging
from logging.handlers import SMTPHandler

from logging.handlers import RotatingFileHandler    # for logging to a file
import os


app = Flask(__name__)
app.config.from_object(Config)  # TO READ CONFIGURATION FROM config file

db = SQLAlchemy(app)  # initialize instance db for database
migrate = Migrate(app=app, db=db)  # initialize instance migrate for migration

login = LoginManager(app)   # initialize instance for LoginManager
login.login_view = 'login' # to let login know which function is handelling i.e, "login"

# FOR SENDING EMAIL OF ERROR- IF DEBUG IS NOT ON
if not app.debug:   # flask var 'debug': which has val of debug: 0|1
    if app.config['MAIL_SERVER']: # checking email server configured or not

        auth = None
        if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
            auth = (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
        secure = None
        if app.config['MAIL_USE_TLS']:
            secure = ()

        mail_handler = SMTPHandler(
            mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
            fromaddr='no-reply@' + app.config['MAIL_SERVER'],
            toaddrs=app.config['ADMINS'], subject='Microblog Failure',
            credentials=auth, secure=secure)
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

    # for logging to a File
    if not os.path.exists('logs'):
        os.mkdir('logs')
    file_handler = RotatingFileHandler('logs/microblog.log', maxBytes=10240,
                                       backupCount=10)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)
    app.logger.info('Microblog startup')

from app import routes, models, errors
